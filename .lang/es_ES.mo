��          �      \      �  �   �     �     �     �     �     �     
          +     0  :   5     p     �     �     �     �     �     �  �   �  �   F         *  !   0     R     m     u     �     �     �     �  P   �     !     :     =     S     Y     s     �  �   �                                                      
                                  	           A small desktop widget that displays the time in the form of a Casio clock. It can also act as a chronometer... and become a Rolex like in the Shakira song... or in a Citizen Eco-Drive that is my watch. I don't have a Rolex. Casio Chronometer:
On/Reset Chronometer:
Start/Stop Citizen Click right button for menu Discard Crono Don't ask me again Exit Help If Casio font was installed it be used on next program run Install Casio Font No Reset Crono Rolex Start/Stop Crono Watch On Yes Your system does Not have the original Casio font installed.Do you want To install it now? (You can do this at any time using the context menu) Project-Id-Version: Casio 3.18.1
PO-Revision-Date: 2023-04-04 10:39 UTC
Last-Translator: jorge <jorge@abu>
Language: es_ES
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
 Un pequeño widget de escritorio que muestra la hora y la fecha en el formato de un reloj Casio. También puede actuar como un cronómetro... y ser cambiado por un Rolex como en la canción de Shakira... o en un Citizen Eco-Drive, que es mi reloj. Yo no tengo un Rolex. Casio Cronómetro:
Establecer/Reiniciar Cronómetro:
Iniciar/Parar Citizen Click botón derecho para menú Descartar Cronómetro No me preguntes otra vez Salir Ayuda Si se instaló la fuente Casio será usada en la proxima ejecución del programa Instalar la fuente Casio No Reiniciar Cronómetro Rolex Iniciar/Parar Cronómetro Mostrar Reloj  Sí Su sistema no tiene instalada la fuente original Casio. ¿Desea instalarla ahora? (Puede instalarla en cualquier momento usando el menú contextual) 